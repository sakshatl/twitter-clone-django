from django.db import models
from django.conf import settings 

# Create your models here.
class Tweet(models.Model):
    body = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete= models.CASCADE,
    )

    def __str__(self):
        return self.body[0:10]

