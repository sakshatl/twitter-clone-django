from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Tweet

# Create your views here.
@login_required(login_url='/accounts/login')
def tweetList(request):
    tweetList = Tweet.objects.all()
    return render(request, 'tweets/tweets.html', {'tweetList': tweetList})
