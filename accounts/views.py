from django.shortcuts import HttpResponse, render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout


# Create your views here.
def signup_view(request):
    if request.method == 'POST':
        form =  UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('http://127.0.0.1:8000/tweets/')
            # and log the user in
    else:
        form = UserCreationForm()
    return render(request, 'accounts/signupForm.html', {'form': form})

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('http://127.0.0.1:8000/tweets/')
            # and log the user in
    else:
        form = AuthenticationForm()
    return render(request, 'accounts/loginForm.html', {'form': form})

def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('http://127.0.0.1:8000/')