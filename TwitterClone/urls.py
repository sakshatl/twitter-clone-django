from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.homepage_view, name='homepage_view'),
    path('tweets/', include('tweets.urls')),
    path('accounts/', include('accounts.urls'))
]


urlpatterns += staticfiles_urlpatterns()